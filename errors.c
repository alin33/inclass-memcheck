#include <stdio.h>
#include <stdlib.h>

void illegalRead() {
  //Illegal read/write
  int x = 0xFFFFFFF;  

  //Use of uninitialised or unaddressable values in system calls
  char* arr  = malloc(10);
  int*  arr2 = malloc(sizeof(int));
  write( 1 /* stdout */, arr, 10 );
  int z;

  //illegal free
  free(&z);

  char * str = malloc(3 * sizeof(char));
  for (int i = 0; i < 4; ++i) str[i] = 'a';
  free(str);
//  free(str);
}

void unitialized() {
  int a=0, b=0, c;
  b = (int*) malloc(sizeof(int));
  free(b);
  free(b); 
}

void fishyValues() {
  char * str = malloc(-3 * sizeof(char));
  free(str);

  //memory leak
  int *q = malloc(sizeof(int));

}

int main() {
  illegalRead();
  fishyValues();
  unitialized();
  return 0;
}
