# OS identification from: https://stackoverflow.com/questions/714100/os-detecting-makefile
OS := $(shell uname -s)

ifeq ($(OS), Darwin) 
  CUNIT_PATH_PREFIX = /usr/local/Cellar/cunit/2.1-3/
  CUNIT_DIRECTORY = cunit
endif
ifeq ($(OS), Linux) 
  CUNIT_PATH_PREFIX = /util/CUnit/
  CUNIT_DIRECTORY = CUnit/
endif

CC = gcc
FLAGS = -g -Wall -std=c11

error: error.c
	$(CC) $(FLAGS) -o errors errors.c
	valgrind --leak-check=yes ./errors

.PHONY: clean

clean:
	rm -rf *~ *.o program tests runner *.dSYM
